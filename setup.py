import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

name = "toast"

setuptools.setup(
    name=name,
    version="0.0.1",
    author="Francisco Hernandez Vivanco",
    author_email="francisco.hernandezvivanco@monash.edu",
    description="Toast: The neuTrOn stAr STacking package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.ligo.org/francisco.hernandez/toast",
    packages=[name],
    package_dir={name: name},
    package_data={name: ["interpolation_points/*.h5", 'tov_data/*',
        'tov_data/maximum_mass_and_sound_model_neural_net/*',
        'tov_data/maximum_mass_and_sound_model_neural_net/assets/*',
        'tov_data/maximum_mass_and_sound_model_neural_net/variables/*']},
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.5',
    install_requires=[
        "bilby",
        "scikit-learn",
        "nestle",
        "tables",
        "lalsuite"]
)
