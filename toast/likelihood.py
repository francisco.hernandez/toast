import os
import numpy as np
from sklearn.ensemble import RandomForestRegressor
import pandas as pd
import bilby
from . import piecewise_polytrope

path_to_interpolation_points = os.path.join(
    os.path.dirname(__file__), 'interpolation_points')

def load_interpolation_points(event):
    """
    Loads the interpolation points

    Parameters
    ----------
    event: str
        Name of a binary neutron star coalesence.
        The events supported are GW190425 and GW170817
  
     Returns
    -------
    dataset: pd.DataFrame
        A pandas dataframe
    """
    dataset = pd.read_hdf(
        "{}/{}.h5".format(path_to_interpolation_points,event))
    return dataset

def train_random_forest(event,train_parameter,pop_parameter):
    """
    Trains a random forest regressor to predict either the
    log_evidence or the log_bayes_factor

    Parameters
    ----------
    event: str
        The events supported are GW170817 and GW190425
    train_parameter: str
        Can be either log_evidence or log bayes factor
    pop parameter: str
        Can be either log_evidence of log_bayes_factor

    Returns
    -------
    random_forest: sklearn.ensemble.RandomForestRegressor
        A random forest regressor
    """
    dataset = pd.read_hdf(
        "{}/{}.h5".format(path_to_interpolation_points,event))
    dataset.pop(pop_parameter)

    train_features = dataset.sample(frac=0.9,random_state=14)
    test_features = dataset.drop(train_features.index)

    train_labels = train_features.pop(train_parameter)
    test_labels = test_features.pop(train_parameter)

    if event == "GW170817":
        seed = 1000
    elif event == "GW190425":
        seed = 14

    # Instantiate model with 50 decision trees 
    random_forest = RandomForestRegressor(
        n_estimators = 50,random_state=seed)
    # Train the model on training data
    random_forest.fit(train_features, train_labels)

    predictions = random_forest.predict(test_features)
    # Calculate the absolute errors
    errors = abs(predictions - test_labels)
    # Calculate the mean absolute error (mae)
    mae = round(np.mean(errors), 2)
    return random_forest

def log_evidence_model(event):
    """
    Trains a random forest regressor that predicts 
    the log evidence given instrinsic parameters
    chirp_mass, mass_ratio, lambda_1, lambda_2

    Parameters
    ----------
    event: str
        Name of a binary neutron star coalesence.
        The events supported are GW190425 and GW170817
  
     Returns
    -------
    random_forest: sklearn.ensemble.RandomForestRegressor
        A random forest regressor

    """
    random_forest = train_random_forest(
        event,'log_evidence','log_bayes_factor')

    return random_forest

def log_bayes_factor_model(event):
    """
    Trains a random forest regressor that predicts 
    the log bayes factor given instrinsic parameters
    chirp_mass, mass_ratio, lambda_1, lambda_2

    Parameters
    ----------
    event: str
        Name of a binary neutron star coalesence.
        The events supported are GW190425 and GW170817
     Returns
    -------
    random_forest: sklearn.ensemble.RandomForestRegressor
        A random forest regressor

    """
    random_forest = train_random_forest(
        event,'log_bayes_factor','log_evidence')

    return random_forest

def get_min_max_allowed_values(event):
    """
    Calculates the minimum and maximum values used for training.
    Evaluating the likelihood (or the bayes factor) outside these 
    values may return innacurate results

    Parameters
    ----------
    event: str
        Name of a binary neutron star coalesence.
        The events supported are GW190425 and GW170817

    Returns
    -------
    min_max_values: dict
        A dictionary containing the minimum and maximum values used for training
    """
    dataset = pd.read_hdf(
        "{}/{}.h5".format(path_to_interpolation_points,event))
    min_max_values = dict(
        min_chirp_mass=min(dataset.chirp_mass), max_chirp_mass=max(dataset.chirp_mass),
        min_mass_ratio=min(dataset.mass_ratio), max_mass_ratio=1,
        min_lambda_1=0, max_lambda_1=max(dataset.lambda_1),
        min_lambda_2=0, max_lambda_2=max(dataset.lambda_2)
        )
    return min_max_values   

class InteropolatedLikelihood(bilby.Likelihood):
    """
    Parameters
    ----------
    interpolated_likelihood: sklearn.ensemble.RandomForestRegressor
        An interplated likelihood

    Returns
    -------
    Likelihood: `bilby.core.likelihood.Likelihood`
        A bilby likelihood object
    """
    def __init__(self, interpolated_likelihood):     
        parameters = dict(chirp_mass=1, mass_ratio=1, lambda_1=0, lambda_2=0)
        bilby.Likelihood.__init__(self, parameters=parameters)
    
        self.interpolated_likelihood = interpolated_likelihood

    def log_likelihood(self):
        intrinsic_parameters = pd.DataFrame(dict(
            chirp_mass=self.parameters["chirp_mass"],
            mass_ratio=self.parameters["mass_ratio"],
            lambda_1=self.parameters["lambda_1"],
            lambda_2=self.parameters["lambda_2"]),
            index=[0])

        log_l = self.interpolated_likelihood.predict(intrinsic_parameters)[0]

        return log_l

class InteropolatedBayesFactor(bilby.Likelihood):
    """
    Parameters
    ----------
    interpolated_bayes_factor: sklearn.ensemble.RandomForestRegressor
        An interplated bayes factor

    Returns
    -------
    Likelihood: `bilby.core.likelihood.Likelihood`
        A bilby likelihood object
    """
    def __init__(self, interpolated_bayes_factor):     
        parameters = dict(chirp_mass=1, mass_ratio=1, lambda_1=0, lambda_2=0)
        bilby.Likelihood.__init__(self, parameters=parameters)
    
        self.interpolated_bayes_factor = interpolated_bayes_factor

    def log_likelihood_ratio(self):
        intrinsic_parameters = pd.DataFrame(dict(
            chirp_mass=self.parameters["chirp_mass"],
            mass_ratio=self.parameters["mass_ratio"],
            lambda_1=self.parameters["lambda_1"],
            lambda_2=self.parameters["lambda_2"]),
            index=[0])

        log_l = \
            self.interpolated_bayes_factor.predict(intrinsic_parameters)[0]

        return log_l


class HyperLikelihood(bilby.Likelihood):
    """
    An hyperlikelihood that samples equation of state hyper-parameters
    assuming the piecewise polytrpe parametrisation.
    See Eq. (9) in https://arxiv.org/pdf/1909.02698.pdf

    Parameters
    ----------
    likelihood_list: list
        A list of interpolated likelihoods
    min_max_values_list: list
        A list containing the minimum and maximum values used for training
        the likelihood distribution
    number_of_grid_points: int
        Number of grid points used for integration
    pressure_array: numpy array
        Pressure array in SI units
    maximum_mass: float
        maxium mass of the EoS determined by pulsar observations in units of solar
        mass, usually 1.97 M_sun
    maximum_speed_of_sound: float
        maximum speed of sound/speed of light (dimensionless)
    
    
    Returns
    -------
    Likelihood: `bilby.core.likelihood.Likelihood`
        A bilby likelihood object
    """ 

    def __init__(self,likelihood_list,min_max_values_list,number_of_grid_points,
            pressure_array, maximum_mass, maximum_speed_of_sound):
        parameters = dict(log_p=33, Gamma_1=2, Gamma_2=2, Gamma_3=2)
        bilby.Likelihood.__init__(self, parameters=parameters)
       
        self.min_max_values_list = min_max_values_list 
        self.number_of_grid_points = number_of_grid_points
        self.mc_vals, self.dA_array, self.q_vals = self.create_grid()
        self.likelihood_list = likelihood_list
        self.pressure_array = pressure_array
        self.maximum_mass = maximum_mass
        self.maximum_speed_of_sound = maximum_speed_of_sound

    def create_grid(self):
        """
        Creates an integration grid between the minimum and maximum values
        allowed by the posterior
        """
        mc_vals = list()
        q_vals = list()
        dA_array = list()
        for min_max in self.min_max_values_list:
            mc_array_tmp = np.linspace(
                min_max["min_chirp_mass"],min_max["max_chirp_mass"],
                self.number_of_grid_points
                )
            q_vals_tmp =  np.linspace(
                min_max["min_mass_ratio"],min_max["max_mass_ratio"], 
                self.number_of_grid_points
                )

            dA_array.append((mc_array_tmp[1]-mc_array_tmp[0]) * \
                (q_vals_tmp[1]-q_vals_tmp[0]))

            q_vals.append(q_vals_tmp)
            mc_vals.append(mc_array_tmp)
            
        return mc_vals, dA_array, q_vals
        
    def eos_model(self,chirp_mass,mass_ratio,log_p,Gamma_1,Gamma_2,Gamma_3,
            min_max_values):
        """
        Computes the tidal deformability

        Parameters
        ----------
        chirp_mass: numpy array
        mass_ratio: numpy array
        log_p: float
        Gamma_1: float
        Gamma_2: float
        Gamma_3: float

        Returns 
        -------
        Lambda: numpy array
            The tidal deformabilites Lambda_1 and Lambda_2
        """
        dict_tmp = dict(chirp_mass=chirp_mass,
                        mass_ratio=mass_ratio)
        tmp = \
           bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters(dict_tmp)

        m1 = tmp[0]['mass_1']
        m2 = tmp[0]['mass_2']
        mass_array = np.array([m1,m2])        
        Lambda, max_mass = piecewise_polytrope.Lambda_of_mass(
            self.pressure_array, mass_array, log_p,
            Gamma_1, Gamma_2, Gamma_3, maximum_mass_limit=self.maximum_mass)
  
        assert min(Lambda[0])>=min_max_values["min_lambda_1"] and \
            max(Lambda[0])<=min_max_values["max_lambda_1"]
        assert min(Lambda[1])>=min_max_values["min_lambda_2"] and \
            max(Lambda[1])<=min_max_values["max_lambda_2"]
        assert max_mass >= self.maximum_mass

        return Lambda[0], Lambda[1]

 
    def integrand(self,chirp_mass,mass_ratio,interpolated_likelihood,
            log_p,Gamma_1,Gamma_2,Gamma_3,min_max_values):
        """
        Function to be integrated

        Parameters
        ----------
        chirp_mass: numpy array
        mass_ratio: numpy array
        interpolated_likelihood: sklearn.ensemble.RandomForestRegressor
        min_max_values: dict
        log_p: float
        Gamma_1: float
        Gamma_2: float
        Gamma_3: float

        Returns 
        -------
        numpy array containing the integrand
            
        """


        l1_tmp, l2_tmp = self.eos_model(
            chirp_mass, mass_ratio, log_p,Gamma_1,Gamma_2,Gamma_3,
            min_max_values
            )
        tests_tmp = pd.DataFrame(dict(
            chirp_mass=chirp_mass*np.ones(len(mass_ratio)),
                                 mass_ratio=mass_ratio,
                                 lambda_1=l1_tmp,
                                 lambda_2=l2_tmp
            ))
        tmp = interpolated_likelihood.predict(tests_tmp)
        tmp2 = np.array(tmp,dtype=np.dtype('float128'))

        return np.exp(tmp2)
           
    def integrate_likelihood(self,interpolated_likelihood,log_p,Gamma_1,
            Gamma_2,Gamma_3,mc_vals_grid,q_vals_grid,min_max_values):
        """
        Integrates the likelihood

        Parameters
        ----------
        interpolated_likelihood: sklearn.ensemble.RandomForestRegressor
        log_p: float
        Gamma_1: float
        Gamma_2: float
        Gamma_3: float
        mc_vals_grid: numpy arrai
        q_vals_grid: numpy assay
        min_max_values: dict
        Returns 
        -------
        result: numpy array
            The integrated lilelihood
        """
        result = 0
        for x in mc_vals_grid:
            result +=np.sum(self.integrand(x, q_vals_grid,interpolated_likelihood,
                            log_p,Gamma_1,Gamma_2,Gamma_3,min_max_values))

        return result
        
    def log_likelihood(self):
        try:
            max_speed_of_sound = piecewise_polytrope.maximum_speed_of_sound(
                self.parameters['log_p'], self.parameters['Gamma_1'],
                self.parameters['Gamma_2'], self.parameters['Gamma_3'])
            
            if max_speed_of_sound<=self.maximum_speed_of_sound:
                integral = list()
                for ii, likelihood in enumerate(self.likelihood_list):
                    integral.append(self.integrate_likelihood(likelihood,
                                self.parameters['log_p'],
                                self.parameters['Gamma_1'],
                                self.parameters['Gamma_2'],
                                self.parameters['Gamma_3'],
                                self.mc_vals[ii],
                                self.q_vals[ii],
                                self.min_max_values_list[ii]))
                log_l = sum(np.log(np.array(integral)*self.dA_array))   
            else:
                log_l = -np.inf
        except (TypeError,AssertionError):
            log_l = -np.inf

        return np.double(log_l) 
