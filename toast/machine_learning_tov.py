import os
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from scipy import optimize
from tqdm import tqdm
from . import piecewise_polytrope

try:
    from tensorflow.keras.models import load_model
    import pickle
except:
    pass

path_to_tov_data = os.path.join(
    os.path.dirname(__file__), 'tov_data')

mass_array = np.array([1.0,1.3,1.6,1.9])
train_stats = pd.read_hdf(
    f'{path_to_tov_data}/maximum_mass_and_sound_model_neural_net_train_stats.h5')

def train_tov_equations():
    '''
    Returns
    -------
    Trained TOV equations
    '''

    random_forest_list = list()

    for mass in tqdm(mass_array):
        tov_data = pd.read_hdf(f'{path_to_tov_data}/{mass}_msun.h5')
        random_forest_list.append(interpolate(tov_data,'Lambda'))

    return random_forest_list

def interpolate(dataset,parameter):
    '''
    Parameters
    ----------
    dataset: pandas dataframe
    parameter: str
        parameter used from training

    Returns
    -------
    rf: sklearn.ensemble.RandomForestRegressor
        A random forest trained model
    '''
    train_features = dataset.sample(frac=0.9,random_state=0)
    test_features = dataset.drop(train_features.index)
    train_labels = train_features.pop(parameter)
    test_labels = test_features.pop(parameter)
    # Instantiate model with 1000 decision trees
    rf = RandomForestRegressor(n_estimators = 50, random_state = 42)
    # Train the model on training data
    rf.fit(train_features, train_labels)
    return rf

def Lambda_from_piecewise_polytrope(
        random_forest_list, mass, log_p,
        Gamma_1, Gamma_2, Gamma_3):
    '''
    Parameters
    ----------
    random_forest_list: list
        a list of trained random forest obtained from train_tov_equations()
    mass: numpy array
        mass in units of solar masses
    log_p, Gamma_1, Gamma_2, Gamma_3 = float
        piecewise polytrope hyper-parameters in SI units
    
    Returns
    -------
    Lambda: numpy array 
        the dimensionless tidal deformability
    '''
    Lambda_tmp = np.zeros(len(mass_array))
    test = pd.DataFrame(dict(log_p=log_p,Gamma_1=Gamma_1,
            gamma_2=Gamma_2,Gamma_3=Gamma_3),index=[0])
        
    for ii in range(len(mass_array)):
        Lambda_tmp[ii] = random_forest_list[ii].predict(test)[0]
    
    params, params_covariance = optimize.curve_fit(
        piecewise_polytrope.tov_fit_function, mass_array, Lambda_tmp,
        p0=[2370, 5.95,-4])
    
    Lambda = piecewise_polytrope.tov_fit_function(mass,params[0],params[1],params[2])
    return Lambda


def load_maximum_mass_and_speed_of_sound_random_forest(
        filename=f'{path_to_tov_data}/maximum_mass_and_sound_model_random_forest.sav'):
    '''
    Returns
    -------
    Trained neural network classifier
    '''
    maximum_mass_and_sound_model = pickle.load(
        open(filename, 'rb'))

    return maximum_mass_and_sound_model


def maximum_mass_and_maximum_speed_of_sound_random_forest(
        trained_model, log_p, Gamma_1, Gamma_2, Gamma_3):
    '''
    We load a trained random forest model to predict if and EoS satisfies 3 conditions:
        m_max >=1.9 m_sun
        speed_of_sound/speed_of_light <=1.15
        lambda <=5000

    Parameters
    ----------
    trained model: keras.load_model
        neural network model of the maximum mass and maximum speed of sound
    log_p, Gamma_1, Gamma_2, Gamma_3 = float
        piecewise polytrope hyper-parameters in SI units
   
    Returns
    -------
    result: float
        1 or 0 
    '''
    test = pd.DataFrame(dict(log_p=log_p,Gamma_1=Gamma_1,
                Gamma_2=Gamma_2,Gamma_3=Gamma_3),index=[0] )
    result = trained_model.predict(test)[0]
  
    return result


def load_maximum_mass_and_speed_of_sound_neural_network_model(
        filename=f'{path_to_tov_data}/maximum_mass_and_sound_model_neural_net'):
    '''
    Returns
    -------
    Trained neural network classifier
    '''

    maximum_mass_and_sound_model = load_model(filename)

    return maximum_mass_and_sound_model


def normalise_data(x):
    '''
    Parameters
    ----------
    x: pandas dataframe
        Unnormalised data
    
    Returns
    -------
    Nomalised data used as input for the neural network classifier
        
    '''
    return (x - train_stats['mean']) / train_stats['std']

def maximum_mass_and_maximum_speed_of_sound_neural_network(
        trained_model, log_p, Gamma_1, Gamma_2, Gamma_3):
    '''
    We train a neural network to predict if and EoS satisfies 3 conditions:
        m_max >=1.9 m_sun
        speed_of_sound/speed_of_light <=1.15
        lambda <=5000

    Parameters
    ----------
    trained model: keras.load_model
        neural network model of the maximum mass and maximum speed of sound
    log_p, Gamma_1, Gamma_2, Gamma_3 = float
        piecewise polytrope hyper-parameters in SI units
   
    Returns
    -------
    result: float
        probability (between 0 and 1) that an EoS satisfies maximum mass and speed
        of sound constraints
    '''
    test = pd.DataFrame(
            dict(log_p=log_p,Gamma_1=Gamma_1,
            Gamma_2=Gamma_2,Gamma_3=Gamma_3),index=[0] )

    result = trained_model.predict(normalise_data(test))[0][0]
    return result
