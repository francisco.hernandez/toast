import lalsimulation as lalsim
from lal import MSUN_SI, G_SI, C_SI
import numpy as np

def Lambda_of_mass(mass,file_path):
    '''
    Parameters
    ----------
    mass = float
        neutron star mass in units of solar masses
    file_path = str
        path of the file containing containing two whitespace separated columns 
        of pressure and density in geometric units 
        
    Returns
    -------
    Lambda = float
        dimensionless tidal deformability Lambda
    '''
    EoS=lalsim.SimNeutronStarEOSFromFile(file_path)
    EoS_family=lalsim.CreateSimNeutronStarFamily(EoS)
    Mass_SI = mass*MSUN_SI #Mass in SI units
    k2=lalsim.SimNeutronStarLoveNumberK2(Mass_SI,EoS_family) #second love number
    R_SI=lalsim.SimNeutronStarRadius(Mass_SI,EoS_family) #radius in SI units
    compactness=G_SI*Mass_SI / ( C_SI**2 *R_SI)
    Lambda=(2/3)*k2/compactness**5
    return Lambda

def Lambda_array_of_mass(mass,file_path):
    '''
    Parameters
    ----------
    mass = numpy array
        neutron star mass in units of solar masses
    file_path = str
        path of the file containing containing two whitespace separated columns 
        of pressure and energy density in geometric units 

    Returns
    -------
    Lambda = float
        dimensionless tidal deformability Lambda
    '''
    Lambda = np.array([Lambda_of_mass(mm,file_path)
                      for mm in mass])
    return Lambda

def radius_of_mass(mass,file_path):
    '''
    Parameters
    ----------
    mass = float 
        neutron star mass in units of solar masses
    file_path = str
        path of the file containing containing two whitespace separated columns 
        of pressure and energy density in geometric units 

    Returns 
    -------
    Radius = float
        Radius in kilometers
    '''
    EoS=lalsim.SimNeutronStarEOSFromFile(file_path)
    EoS_family=lalsim.CreateSimNeutronStarFamily(EoS)
    Mass_SI = mass*MSUN_SI #Mass in SI units
    R_SI=lalsim.SimNeutronStarRadius(Mass_SI,EoS_family) #radius in SI units
    return R_SI/1000

def radius_array_of_mass(mass,file_path):
    '''
    Parameters
    ----------
    mass = numpy array
        neutron star mass in units of solar masses
    file_path = str
        path of the file containing containing two whitespace separated columns 
        of pressure and energy density in geometric units 
    
    Returns 
    -------
    Radius = numpy array
        Radius in kilometers

    '''
    R_SI = np.array([radius_of_mass(mm,file_path)
                      for mm in mass])
    
    return R_SI

def maximum_mass(
        file_path):
    '''
    Parameters
    ----------
    file_path = str
        path of the file containing containing two whitespace separated columns 
        of pressure and energy density in geometric units 
    
    Returns
    -------
    max_mass = float 
        the maximum mass in solar masses
    '''
    EoS=lalsim.SimNeutronStarEOSFromFile(file_path)
    EoS_family=lalsim.CreateSimNeutronStarFamily(EoS)
    
    max_mass = lalsim.SimNeutronStarMaximumMass(EoS_family)/MSUN_SI
    return max_mass
