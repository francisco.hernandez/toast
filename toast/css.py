from . import piecewise_polytrope
import numpy as np
import lalsimulation as lalsim
from lal import C_SI

def energy_density_of_pressure(
        central_pressure,delta_epsilon,speed_of_sound,
        transition_pressure,log_p,Gamma_1,Gamma_2):
    '''
    Eq. (14) in https://arxiv.org/pdf/1512.09183.pdf
    
    Parameters
    ----------
    central_pressure: numpy array
        central pressure [Pa]
    delta_epsilon: float
        delta energy density [J/m^3]
    speed_of_sound: float
        dimensionless, ranges between 0 and 1, where 1 corresponds to the speed of light 
    transition_pressure: float
        transition pressure [Pa]
    log_p, Gamma_1, Gamma_2: float
        piecewise polytrope hyper-parameters. We use 2 polytropes
    
    Returns
    -------
    energy density [J/m^3]
    '''
     
    
    
    args_below = np.argwhere(
        central_pressure<transition_pressure).flatten()
    args_above = np.argwhere(
        central_pressure>transition_pressure).flatten()
    
    
    # We use two polytropes for the low density EoS, 
    # therefore Gamma_3=Gamma_2
    energy_density_nuclear_matter = piecewise_polytrope.energy_density_of_pressure(
        central_pressure[args_below],log_p,Gamma_1,Gamma_2,Gamma_2)
    
    energy_density_quark_matter = energy_density_nuclear_matter[-1] + delta_epsilon + \
        speed_of_sound**(-2)*(central_pressure[args_above] - transition_pressure)

    return np.append(energy_density_nuclear_matter,energy_density_quark_matter)

def density_of_pressure(
        central_pressure,delta_epsilon,speed_of_sound,
        transition_pressure,log_p,Gamma_1,Gamma_2):
    '''
    Eq. (14) in https://arxiv.org/pdf/1512.09183.pdf
    
    Parameters
    ----------
    central_pressure: numpy array
        central pressure [Pa]
    delta_epsilon: float
        delta energy density [kg/m^3]
    speed_of_sound: float
        dimensionless, ranges between 0 and 1, where 1 corresponds to the speed of light 
    transition_pressure: float
        transition pressure [Pa]
    log_p, Gamma_1, Gamma_2: float
        piecewise polytrope hyper-parameters. We use 2 polytropes
    
    Returns
    -------
    rest mass density: numpy array
        rest mass density [kg/m^3]
    '''
     
    
    args_below = np.argwhere(
        central_pressure<transition_pressure).flatten()
    args_above = np.argwhere(
        central_pressure>transition_pressure).flatten()
    
    
    # We use two polytropes for the low density EoS, 
    # therefore Gamma_3=Gamma_2
    energy_density_nuclear_matter = piecewise_polytrope.density_of_pressure(
        central_pressure[args_below],log_p,Gamma_1,Gamma_2,Gamma_2)
    
    energy_density_quark_matter = energy_density_nuclear_matter[-1] + delta_epsilon + \
        (speed_of_sound*C_SI)**(-2)*(central_pressure[args_above] - transition_pressure)

    return np.append(energy_density_nuclear_matter,energy_density_quark_matter)
    
