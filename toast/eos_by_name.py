import lalsimulation as lalsim
from lal import MSUN_SI, G_SI, C_SI
import numpy as np

def Lambda_of_mass(mass,eos_name):
    '''
    Parameters
    ----------
    mass = float
        neutron star mass in units of solar masses
    eos_name = str
        Name of the EoS, for example sly
        
    Returns
    -------
    Lambda = float
        dimensionless tidal deformability Lambda
    '''
    EoS=lalsim.SimNeutronStarEOSByName(eos_name)
    EoS_family=lalsim.CreateSimNeutronStarFamily(EoS)
    Mass_SI = mass*MSUN_SI #Mass in SI units
    k2=lalsim.SimNeutronStarLoveNumberK2(Mass_SI,EoS_family) #second love number
    R_SI=lalsim.SimNeutronStarRadius(Mass_SI,EoS_family) #radius in SI units
    compactness=G_SI*Mass_SI / ( C_SI**2 *R_SI)
    Lambda=(2/3)*k2/compactness**5
    return Lambda

def Lambda_array_of_mass(mass,eos_name):
    '''
    Parameters
    ----------
    mass = numpy array
        neutron star mass in units of solar masses
    eos_name = str
        Name of the EoS, for example sly

    Returns
    -------
    Lambda = float
        dimensionless tidal deformability Lambda
    '''
    Lambda = np.array([Lambda_of_mass(mm,eos_name)
                      for mm in mass])
    return Lambda

def radius_of_mass(mass,eos_name):
    '''
    Parameters
    ----------
    mass = float 
        neutron star mass in units of solar masses
    eos_name = str
        Name of the EoS, for example sly

    Returns 
    -------
    Radius = float
        Radius in kilometers
    '''
    EoS=lalsim.SimNeutronStarEOSByName(eos_name)
    EoS_family=lalsim.CreateSimNeutronStarFamily(EoS)
    Mass_SI = mass*MSUN_SI #Mass in SI units
    R_SI=lalsim.SimNeutronStarRadius(Mass_SI,EoS_family) #radius in SI units
    return R_SI/1000

def radius_array_of_mass(mass,eos_name):
    '''
    Parameters
    ----------
    mass = numpy array
        neutron star mass in units of solar masses
    eos_name = str
        Name of the EoS, for example sly

    Returns 
    -------
    Radius = numpy array
        Radius in kilometers

    '''
    R_SI = np.array([radius_of_mass(mm,eos_name)
                      for mm in mass])
    
    return R_SI

def maximum_mass(eos_name):
    '''
    Parameters
    ----------
    eos_name = str
        Name of the EoS, for example sly
    
    Returns
    -------
    max_mass = float 
        the maximum mass in solar masses
    '''
    EoS=lalsim.SimNeutronStarEOSByName(eos_name)
    EoS_family=lalsim.CreateSimNeutronStarFamily(EoS)
    
    max_mass = lalsim.SimNeutronStarMaximumMass(EoS_family)/MSUN_SI
    return max_mass

def density_of_pressure(pressure,eos_name):
    '''
    Parameters
    pressure = numpy array
        pressure in Pa
    eos_name = str
        Name of the EoS name, for example sly
    
    Returns
    -------
    density = numpy array
        density in kg/m^3
    '''
    EoS=lalsim.SimNeutronStarEOSByName(eos_name)
    enthalpy = np.array([lalsim.SimNeutronStarEOSPseudoEnthalpyOfPressure(pp,EoS)
                    for pp in pressure])
    density = np.array([lalsim.SimNeutronStarEOSRestMassDensityOfPseudoEnthalpy(ee,EoS)
                  for ee in enthalpy])
    
    return density 

def energy_density_of_pressure(
    pressure,eos_name):
    '''
    Parameters
    ----------
    pressure = numpy array
        pressure array in SI units
    eos_name = str
        Name of the EoS name, for example sly
   
    Returns
    -------
    energy density = numpy array
        energy density in J/m^3
    '''
    EoS = lalsim.SimNeutronStarEOSByName(eos_name)

    energy_density = np.array([lalsim.SimNeutronStarEOSEnergyDensityOfPressure(pp,EoS)
                  for pp in pressure])
    
    return energy_density
