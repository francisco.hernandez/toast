# Toast

Toast: The neuTrOn stAr STacking package

## Installation instructions
```shell
git clone git@git.ligo.org:francisco.hernandez/toast.git
cd toast
pip install -r requirements.txt
pip install .
```

## Citation guide
If you use `Toast` for a scientific publication, please cite
* [A scalable random forest regressor for combining neutron-star equation of state measurements: A case study with GW170817 and GW190425](https://ui.adsabs.harvard.edu/abs/2020arXiv200805627H/abstract)

or if you use our neutron star stacking method, please cite
* [Measuring the neutron star equation of state with gravitational waves: The first forty binary neutron star merger observations](https://ui.adsabs.harvard.edu/abs/2019PhRvD.100j3009H/abstract)

We also use the following packages that you should consider citing if you use them:
* [Bilby](https://git.ligo.org/lscsoft/bilby/-/tree/master)
* [LALSuite](https://lscsoft.docs.ligo.org/lalsuite/)
* [Scikit-learn](https://scikit-learn.org/stable/index.html)
* [Dynesty](https://dynesty.readthedocs.io/en/latest/)
* [Nestle](http://kylebarbary.com/nestle/)

## Basic usage
First, we import `toast` and `pandas`

```python
import toast
import pandas as pd
```

We then train a random forest regressor to predict the `log_evidence` (this is essentially the marginalised `log_likelihood`). The gravitational-wave events supported are GW170817 and GW190425.
```python
interpolated_likelihood = toast.likelihood.log_evidence_model("GW170817")
```

We finally predict the `log_evidence` given the intrinsic parameters `chirp_mass`, `mass_ratio`, `lambda_1` and `lambda_2`. The intrinsic parameters can be either a pandas dataframe or a numpy array.

```python
intrinsic_parameters = pd.DataFrame(dict(
  chirp_mass=1.19,
  mass_ratio=0.8,
  lambda_1=100,
  lambda_2=200),
  index=[0])
  
log_evidence = interpolated_likelihood.predict(intrinsic_parameters)
```

Note that we only train the model between the minimum and maximum values supported by the posterior. These values can be easily obtained:
```python
min_max_values = toast.likelihood.get_min_max_allowed_values("GW170817")
```
To see how we use the interpolated likelihoods in practice, refer to the `examples` folder.

## Interpolation points
We can load the interpolation points used for training
```python
import toast

interpolation_points = toast.likelihood.load_interpolation_points("GW170817")
```

The supported events are GW170817 and GW190425. Alternatively, the interpolation points can be found in `toast/interpolation_points`
