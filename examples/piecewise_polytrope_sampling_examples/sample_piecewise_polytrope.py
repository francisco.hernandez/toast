"""
Tutorial to demonstrate running parameter estimation on a binary neutron star
system including the piecewise polytrope parameterisation of the
equation of state.
"""

import toast
import bilby
import numpy as np

outdir='outdir'
label='sample_piecewise_polytrope'

# Set up a random seed for result reproducibility.  This is optional!
np.random.seed(170817)

# The piecewise polytrope parameters are included here
injection_parameters = dict(
    chirp_mass=1.215, mass_ratio=0.86, chi_1=0.02, chi_2=0.02, luminosity_distance=50,
    theta_jn=0.4, psi=2.659, phase=1.3, geocent_time=100,
    ra=1.375, dec=-1.2108, log_p=33.4, Gamma_1=3.005, Gamma_2=2.988,
    Gamma_3=2.851)

# Set the duration and sampling frequency of the data segment that we're going
# to inject the signal into.
duration = 4
sampling_frequency = 2 * 1024
start_time = injection_parameters['geocent_time'] + 0.2 - duration

# Fixed arguments passed into the source model. The analysis starts at 50 Hz.
# Note that here we include an array of central pressures
waveform_arguments = dict(waveform_approximant='IMRPhenomPv2_NRTidal',
                          reference_frequency=50., minimum_frequency=100.0,
                          central_pressure_array = np.logspace(np.log10(1e31),np.log10(2.5e35),100))

# Create the waveform_generator using a LAL Binary Neutron Star source function
waveform_generator = bilby.gw.WaveformGenerator(
    duration=duration, sampling_frequency=sampling_frequency,
    frequency_domain_source_model=toast.source.lal_binary_neutron_star_piecewise_polytrope,
    parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters,
    waveform_arguments=waveform_arguments)

# Set up interferometers.  In this case we'll use three interferometers
# (LIGO-Hanford (H1), LIGO-Livingston (L1), and Virgo (V1)).
# These default to their design sensitivity and start at 50 Hz.
interferometers = bilby.gw.detector.InterferometerList(['H1', 'L1', 'V1'])
for interferometer in interferometers:
    interferometer.minimum_frequency = 100
interferometers.set_strain_data_from_power_spectral_densities(
    sampling_frequency=sampling_frequency, duration=duration,
    start_time=start_time)
interferometers.inject_signal(parameters=injection_parameters,
                              waveform_generator=waveform_generator)

# We next define the constraints used when sampling the piecewise polytrope
# These include  maximum mass and causality constraints
def eos_constraints(parameters):
    if hasattr(parameters['log_p'], "__len__"):
        log_p = parameters['log_p'][0]
        Gamma_1 = parameters['Gamma_1'][0]
        Gamma_2 = parameters['Gamma_2'][0]
        Gamma_3 = parameters['Gamma_3'][0]
    else:
        log_p = parameters['log_p']
        Gamma_1 = parameters['Gamma_1']
        Gamma_2 = parameters['Gamma_2']
        Gamma_3 = parameters['Gamma_3']
    
    maximum_eos_mass = toast.piecewise_polytrope.maximum_mass(
        log_p=log_p,Gamma_1=Gamma_1,Gamma_2=Gamma_2,Gamma_3=Gamma_3)
    parameters['maximum_eos_mass'] = maximum_eos_mass
    
    maximum_speed_of_sound = toast.piecewise_polytrope.maximum_speed_of_sound(
        log_p=log_p,Gamma_1=Gamma_1,Gamma_2=Gamma_2,Gamma_3=Gamma_3)
    parameters['maximum_speed_of_sound'] = maximum_speed_of_sound
     
    return parameters

# We use the bilby default BNSPriorDict including the eos constraints
# defined in the function above.
# We also define the piecewise polytrope priors
priors = bilby.gw.prior.BNSPriorDict(conversion_function=eos_constraints)
priors['log_p'] = bilby.prior.Uniform(32.6,33.8,name='log_p',latex_label='$\\log p_0$')
priors['Gamma_1'] = bilby.prior.Uniform(2,4.5,name='Gamma_1',latex_label='$\\Gamma_1$')
priors['Gamma_2'] = bilby.prior.Uniform(1.1,4.5,name='Gamma_2',latex_label='$\\Gamma_2$')
priors['Gamma_3'] = bilby.prior.Uniform(1.1,4.5,name='Gamma_3',latex_label='$\\Gamma_3$')

# We set up the limits on causuality and maximum mass
priors['maximum_eos_mass'] = bilby.core.prior.Constraint(minimum=1.97,maximum=5)
priors['maximum_speed_of_sound'] = bilby.core.prior.Constraint(minimum=0,maximum=1.15)
priors['lambda_1'] = bilby.core.prior.Constraint(minimum=0,maximum=5000)
priors['lambda_2'] = bilby.core.prior.Constraint(minimum=0,maximum=5000)

# Set up a prior for chirp_mass and mass_ratio
priors['chirp_mass'] = bilby.core.prior.Uniform(minimum=1.20, maximum=1.22, name='chirp_mass', unit='$M_{\\odot}$')
priors['mass_ratio'] = bilby.core.prior.Uniform(minimum=0.7,maximum=1,name='mass_ratio')

# Fix other parameters
for key in ['psi', 'geocent_time', 'ra', 'dec', 'chi_1', 'chi_2',
            'theta_jn', 'luminosity_distance', 'phase']:
    priors[key] = injection_parameters[key]

# Initialise the likelihood by passing in the interferometer data (IFOs)
# and the waveform generator
likelihood = bilby.gw.GravitationalWaveTransient(
    interferometers=interferometers, waveform_generator=waveform_generator,
    time_marginalization=False, phase_marginalization=False,
    distance_marginalization=False, priors=priors)

# Run sampler.  In this case we're going to use the dynesty sampler
result = bilby.run_sampler(
    likelihood=likelihood, priors=priors, sampler='dynesty', walks=30, npoints=200,
    injection_parameters=injection_parameters, outdir=outdir, label=label,
    check_point_plot=False)

result.plot_corner()

# We finaly generate all bns samples, including lambda_1 and lambda_2 from the 
# piecewise polytrope posteirors
posteriors = result.posterior
all_bns_posteriors = toast.piecewise_polytrope.generate_bns_parameters_from_piecewise_polytrope_posteriors(
    posteriors) 
all_bns_posteriors.to_hdf(f'{outdir}/{label}_all_posterior_samples.h5',key='posteriors')
