"""
Tutorial to demonstrate running parameter estimation on a neutron star-black hole
system including the piecewise polytrope parameterisation of the
equation of state.
"""

import toast
import bilby
import numpy as np
import pandas as pd

outdir='outdir'
label='nsbh'

# Set up a random seed for result reproducibility.  This is optional!
np.random.seed(170817)

# The piecewise polytrope parameters are included here
injection_parameters = dict(
    chirp_mass=3.34, mass_ratio=0.25, chi_1=0.02, chi_2=0.02, luminosity_distance=50,
    theta_jn=0.4, psi=2.659, phase=1.3, geocent_time=100,
    ra=1.375, dec=-1.2108, log_p=33.4, Gamma_1=3.005, Gamma_2=2.988,
    Gamma_3=2.851)

# Set the duration and sampling frequency of the data segment that we're going
# to inject the signal into.
duration = 64
sampling_frequency = 2 * 2048
start_time = injection_parameters['geocent_time'] + 0.2 - duration

# Fixed arguments passed into the source model. The analysis starts at 50 Hz.
# Note that here we include an array of central pressures
waveform_arguments = dict(waveform_approximant='IMRPhenomNSBH',
                          reference_frequency=50., minimum_frequency=20.0,
                          central_pressure_array = np.logspace(np.log10(1e33),np.log10(100e33),40))

# Create the waveform_generator using a LAL Binary Neutron Star source function
waveform_generator = bilby.gw.WaveformGenerator(
    duration=duration, sampling_frequency=sampling_frequency,
    frequency_domain_source_model=toast.source.lal_neutron_star_black_hole_piecewise_polytrope,
    parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters,
    waveform_arguments=waveform_arguments)

# Set up interferometers.  In this case we'll use three interferometers
# (LIGO-Hanford (H1), LIGO-Livingston (L1), and Virgo (V1)).
# These default to their design sensitivity and start at 50 Hz.
interferometers = bilby.gw.detector.InterferometerList(['H1', 'L1', 'V1'])
for interferometer in interferometers:
    interferometer.minimum_frequency = 20
interferometers.set_strain_data_from_power_spectral_densities(
    sampling_frequency=sampling_frequency, duration=duration,
    start_time=start_time)
interferometers.inject_signal(parameters=injection_parameters,
                              waveform_generator=waveform_generator)

# We next define the constraints used when sampling the piecewise polytrope
# These include  maximum mass and causality constraints
def eos_constraints(parameters):
    if hasattr(parameters['log_p'], "__len__"):
        log_p = parameters['log_p'][0]
        Gamma_1 = parameters['Gamma_1'][0]
        Gamma_2 = parameters['Gamma_2'][0]
        Gamma_3 = parameters['Gamma_3'][0]
    else:
        log_p = parameters['log_p']
        Gamma_1 = parameters['Gamma_1']
        Gamma_2 = parameters['Gamma_2']
        Gamma_3 = parameters['Gamma_3']
    
    maximum_eos_mass = toast.piecewise_polytrope.maximum_mass(
        log_p=log_p,Gamma_1=Gamma_1,Gamma_2=Gamma_2,Gamma_3=Gamma_3)
    parameters['maximum_eos_mass'] = maximum_eos_mass
    
    maximum_speed_of_sound = toast.piecewise_polytrope.maximum_speed_of_sound(
        log_p=log_p,Gamma_1=Gamma_1,Gamma_2=Gamma_2,Gamma_3=Gamma_3)
    parameters['maximum_speed_of_sound'] = maximum_speed_of_sound
     
    return parameters

# We use the bilby default BNSPriorDict including the eos constraints
# defined in the function above.
# We also define the piecewise polytrope priors
priors = bilby.gw.prior.BNSPriorDict(conversion_function=eos_constraints)
priors['log_p'] = bilby.prior.Uniform(32.6,33.8,name='log_p',latex_label='$\\log p_0$')
priors['Gamma_1'] = bilby.prior.Uniform(2,4.5,name='Gamma_1',latex_label='$\\Gamma_1$')
priors['Gamma_2'] = bilby.prior.Uniform(1.1,4.5,name='Gamma_2',latex_label='$\\Gamma_2$')
priors['Gamma_3'] = bilby.prior.Uniform(1.1,4.5,name='Gamma_3',latex_label='$\\Gamma_3$')

# We set up the limits on causuality and maximum mass
priors['maximum_eos_mass'] = bilby.core.prior.Constraint(minimum=1.9,maximum=5)
priors['maximum_speed_of_sound'] = bilby.core.prior.Constraint(minimum=0,maximum=1.15)
priors['lambda_1'] = 0
priors['lambda_2'] = bilby.core.prior.Constraint(minimum=0,maximum=5000)

# Set up a prior for chirp_mass and mass_ratio
priors['chirp_mass'] = bilby.core.prior.Uniform(minimum=2.8, maximum=3.5, name='chirp_mass', unit='$M_{\\odot}$')
priors['mass_ratio'] = bilby.core.prior.Uniform(minimum=0.125,maximum=0.45,name='mass_ratio')
priors['geocent_time'] = bilby.core.prior.Uniform(minimum=100-0.05,maximum=100+0.05,name='geocent_time')
priors['mass_1'] = bilby.core.prior.Constraint(minimum=0.5, maximum=20, name='mass_1', latex_label='$m_1$', unit=None)
priors['mass_2'] = bilby.core.prior.Constraint(minimum=0.5, maximum=3, name='mass_2', latex_label='$m_2$', unit=None)

# Fix other parameters
for key in ['psi', 'ra', 'dec', 'chi_1', 'chi_2',
            'theta_jn', 'luminosity_distance']:
    priors[key] = injection_parameters[key]

# Initialise the likelihood by passing in the interferometer data (IFOs)
# and the waveform generator
likelihood = bilby.gw.GravitationalWaveTransient(
    interferometers=interferometers, waveform_generator=waveform_generator,
    time_marginalization=False, phase_marginalization=True,
    distance_marginalization=False, priors=priors)

# Run sampler.  In this case we're going to use the dynesty sampler
result = bilby.run_sampler(
    likelihood=likelihood, priors=priors, sampler='nestle', npoints=500,
    injection_parameters=injection_parameters, outdir=outdir, label=label,
    check_point_plot=False)

result.plot_corner()

mass, radius = toast.piecewise_polytrope.mass_radius_posterior(result)
df1 = pd.DataFrame(dict(mass=mass,radius=radius))
df1.to_hdf(f'{outdir}/{label}_mass_radius_posteriors.h5',key='posterior')

density, pressure = toast.piecewise_polytrope.pressure_density_posterior(result)
df2 = pd.DataFrame(dict(density=density,pressure=pressure))
df2.to_hdf(f'{outdir}/{label}_pressure_density_posteriors.h5',key='posterior')
