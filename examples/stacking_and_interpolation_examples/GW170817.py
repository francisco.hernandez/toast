#!/usr/bin/env python
"""
Tutorial to demonstrate running parameter estimation using a marginalised
interpolated likelihood of GW170817.

We train a random forest regressor using scikit-learn  to predict the 
log evidence given the chirp_mass, mass_ratio, lambda_1 and lambda_2.
"""
import pandas as pd
import bilby
from toast import likelihood

outdir = "outdir"
label = "GW170817"

# We train a  random forest to predict the log_evidence. 
# The result is a RandomForestRegressor class. See the link below for more info:
# https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestRegressor.html
interpolated_likelihood = likelihood.log_evidence_model("GW170817")

# We the load the miminum and maximum values used for training.
# Evaluating the likelihood outside these values may result in
# innacurate results
min_max_values = likelihood.get_min_max_allowed_values("GW170817")
    
# We set up the likelihood
likelihood = likelihood.InteropolatedLikelihood(interpolated_likelihood)

# We make a prior between the minimum and 
# maximum values of the intinsic parameters 
# supported by the posterior of GW170817
priors = dict()
priors["chirp_mass"] = bilby.prior.Uniform(
    minimum=min_max_values["min_chirp_mass"], 
    maximum=min_max_values["max_chirp_mass"], 
    name="chirp_mass")
priors["mass_ratio"] = bilby.prior.Uniform(
    minimum=min_max_values["min_mass_ratio"], 
    maximum=min_max_values["max_mass_ratio"], 
    name="mass_ratio")
priors["lambda_1"] = bilby.prior.Uniform(
    minimum=min_max_values["min_lambda_1"], 
    maximum=min_max_values["max_lambda_1"],
    name="lambda_1")
priors["lambda_2"] = bilby.prior.Uniform(
    minimum=min_max_values["min_lambda_2"], 
    maximum=min_max_values["max_lambda_2"],
    name="lambda_2")

# We run the sampler, in this case we use the nestle sampler
result = bilby.run_sampler(
    likelihood=likelihood, priors=priors, sampler="dynesty", 
    npoints=2000, label=label,outdir=outdir) 
result.plot_corner()
