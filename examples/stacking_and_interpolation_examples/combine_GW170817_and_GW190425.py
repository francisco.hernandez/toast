'''
Tutorial to demonstrate how to combine GW170817 and GW190425 
to constrain the equation of state using toast
'''

import numpy as np
import pandas as pd
import bilby
import toast

outdir = "outdir"
label = "eos_stacking"

# We interpolate the GW170817 and GW190425 likelihoods.
# We also load the minimum and maximum values used for training
GW190425 = toast.likelihood.log_bayes_factor_model("GW190425")
min_max_values_GW190425 = toast.likelihood.get_min_max_allowed_values("GW190425")

GW170817 = toast.likelihood.log_bayes_factor_model("GW170817")
min_max_values_GW170817 = toast.likelihood.get_min_max_allowed_values("GW170817")

likelihood_list = [GW170817, GW190425]
min_max_values_list = [min_max_values_GW170817, min_max_values_GW190425]

# We pass a list of interpolated likelihoods to the HyperLikelihood
# The hyper-likelihood is defined in Eq. (9) of https://arxiv.org/abs/1909.02698
# We use the piecewise polytrope parametrisation of the EoS and 
# reject samples where speed_of_sound/speed_of_light > 1.1 
# and TOV_mass < 1.97 M_sun
likelihood = toast.likelihood.HyperLikelihood(
    likelihood_list=likelihood_list,
    min_max_values_list=min_max_values_list,
    number_of_grid_points=50,
    pressure_array=np.logspace(np.log10(4e32),np.log10(2.5e35),100),
    maximum_mass=1.97, maximum_speed_of_sound=1.1)

# We set up a prior
priors = dict()
priors["log_p"]=bilby.prior.Uniform(
    32.6,34.4,name='log_p',latex_label="$\\log p_0$")
priors["Gamma_1"]=bilby.prior.Uniform(
    2,4.5,name='Gamma_1',latex_label="$\\Gamma_1$")
priors["Gamma_2"]=bilby.prior.Uniform(
    1.1,4.5,name='Gamma_2',latex_label="$\\Gamma_2$")
priors["Gamma_3"]=bilby.prior.Uniform(
    1.1,4.5,name='Gamma_3',latex_label="$\\Gamma_3$")

# We then run the sampler
result = bilby.run_sampler(
    likelihood=likelihood, priors=priors, 
    sampler="nestle", npoints=500, 
    outdir=outdir,label=label)
result.plot_corner()

# Finally we convert the polytrope posteriors to
# mass-radius and pressure density posteriors
mass, radius = toast.piecewise_polytrope.mass_radius_posterior(result)
df1 = pd.DataFrame(dict(mass=mass,radius=radius))
df1.to_hdf(f'{outdir}/{label}_mass_radius_posteriors.h5',key='posterior')

density, pressure = toast.piecewise_polytrope.pressure_density_posterior(result)
df2 = pd.DataFrame(dict(density=density,pressure=pressure))
df2.to_hdf(f'{outdir}/{label}_pressure_density_posteriors.h5',key='posterior')

# We can additionally calculate the 90% confidence intervals
min_radius, max_radius, median_radius, mass = toast.piecewise_polytrope.mass_radius_confidence_intervals(df1)
min_density, max_density, median_density, pressure = toast.piecewise_polytrope.pressure_density_confidence_intervals(df2)

